import {makeStateKey, TransferState} from "@angular/platform-browser";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {Injectable} from "@angular/core";
import {AuthenticationService} from "./services/authentication.service";

@Injectable({
  providedIn: "root"
})
export class BrowserStateInterceptor implements HttpInterceptor {

  constructor(
    private readonly transferState: TransferState,
    private readonly authenticationService: AuthenticationService
  ) { }

  intercept(req: HttpRequest<object>, next: HttpHandler): Observable<HttpEvent<object>> {
    if (req.method !== "GET") {
      return next.handle(req);
    }

    const key: string & { __not_a_string: never } = makeStateKey(req.url + req.params.toString());
    const storedResponse: null = this.transferState.get(key, null);

    if (storedResponse) {
      this.transferState.remove(key);
      const response: HttpResponse<string> = new HttpResponse({ body: storedResponse, status: 200 });
      // @ts-ignore
      return of(response);
    }

    return next.handle(req);
  }
}
