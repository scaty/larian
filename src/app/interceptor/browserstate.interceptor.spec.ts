import {TransferState} from "@angular/platform-browser";
import {BrowserStateInterceptor} from "./browserstate.interceptor";
import {TestBed} from "@angular/core/testing";
import {HttpClientModule} from "@angular/common/http";
import {CoreModule} from "../core/core.module";
import {AmStoreModule} from "../store/am-store.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {RouterTestingModule} from "@angular/router/testing";
import {AuthenticationService} from "./services/authentication.service";
import {CookieService} from "./services/cookie.service";
import {LoaderService} from "./services/loader.service";

describe("browserstate.interceptor", () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule, CoreModule, AmStoreModule, BrowserAnimationsModule, RouterTestingModule.withRoutes([])],
    providers: [ AuthenticationService, { provide: "req", useValue: null }, TransferState, CookieService, LoaderService]
  }));
  it("should be created", () => {
    const browserStateInterceptor: BrowserStateInterceptor = TestBed.inject(BrowserStateInterceptor);
    expect(browserStateInterceptor).toBeTruthy();
  });
});
