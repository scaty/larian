import {Directive, Input} from "@angular/core";
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, Validators} from '@angular/forms';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: "[min]",
  providers: [{ provide: NG_VALIDATORS, useExisting: MinDirective, multi: true }]
})
export class MinDirective implements Validator {

  @Input() min: number = 0;

  validate(control: AbstractControl): ValidationErrors | null {
    return Validators.min(this.min)(control);
  }
}
