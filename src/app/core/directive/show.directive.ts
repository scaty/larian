import {Directive, ElementRef, Input, OnInit, Renderer2} from "@angular/core";
import {UnsubscribeOnDestroy} from "../utl/unsubscribe-on-destroy";
import {combineLatest} from "rxjs";
import {filter, takeUntil} from "rxjs/operators";
import {ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {EntityCache} from "@ngrx/data";

@Directive({
  selector: "[amShow]"
})
export class ShowDirective extends UnsubscribeOnDestroy implements OnInit {
  @Input("amShow") model: string = "";
  @Input() parent: string = "mat-form-field";
  constructor(private readonly element: ElementRef,
              private readonly renderer: Renderer2,
              private readonly route: ActivatedRoute,
              private readonly store: Store<EntityCache>, ) { super(); }

  ngOnInit(): void {
    const courseUUID: string | null = this.route.snapshot.paramMap.get("lectureuuid");
    const moduleStudyUUID: string | null = this.route.snapshot.paramMap.get("moduleuuid");
    const studyUUID: string | null = this.route.snapshot.paramMap.get("uuid");
    combineLatest([
      this.store.select(selectStudyByUuid(studyUUID)).pipe(filter(o => !!o)),
      this.store.select(selectModuleStudyByUuid(moduleStudyUUID)),
      this.store.select(selectCourseByUuid(courseUUID)),
      this.store.select(selectChoices)]
    ).pipe(takeUntil(this.componentDestroyed)).subscribe(([study, module, course, choices]) => {
      const lang: string = course?.lang ? course.lang : module?.lang ? module.lang : study.lang;
      if (choices.find(o => o.uuid === lang)?.identifier === "avw_ch_sprache_e" && this.model === "de" ||
          choices.find(o => o.uuid === lang)?.identifier === "avw_ch_sprache_d" && this.model === "en"
      ) {
        this.setClass();
      } else {
        this.removeClass();
      }
    });
  }

  private removeClass(): void {
    const element = this.parent !== "" ? this.element.nativeElement.closest(this.parent) : this.element.nativeElement;
    this.renderer.removeClass(element, "hide");
  }

  private setClass(): void {
    const element = this.parent !== "" ? this.element.nativeElement.closest(this.parent) : this.element.nativeElement;
    this.renderer.addClass(element, "hide");
  }

}
