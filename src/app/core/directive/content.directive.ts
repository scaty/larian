import {Directive, HostBinding} from "@angular/core";

@Directive({
  selector: "[amContent]",
})
export class ContentDirective {
  @HostBinding("class.content-section-new")
  variableName: boolean = true;
  constructor() { }

}
