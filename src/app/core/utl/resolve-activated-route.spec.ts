import {ResolveActivatedRoute} from "./resolve-activated-route";
import {ActivatedRouteSnapshot, ParamMap, UrlSegment} from "@angular/router";
import {TestBed} from "@angular/core/testing";

const mockSnapshot: any = jasmine.createSpyObj<ActivatedRouteSnapshot>("ActivatedRouteSnapshot", ["toString"]);

describe("ResolveActivatedRoute", () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: []
  }).compileComponents());

  it("return url", () => {
    // given
    const resolve: ResolveActivatedRoute = new ResolveActivatedRoute();
    let activatedRouteSnapshot: ActivatedRouteSnapshot = new ActivatedRouteSnapshot();
    const activatedRouteSnapshotSelf: ActivatedRouteSnapshot = new ActivatedRouteSnapshot();
    const activatedRouteSnapshotChild: ActivatedRouteSnapshot = new ActivatedRouteSnapshot();
    const urlSegment: UrlSegment = new UrlSegment("route1", {});
    const urlSegmentSecond: UrlSegment = new UrlSegment("route2", {});
    activatedRouteSnapshotSelf.url = new Array<UrlSegment>(urlSegment);
    activatedRouteSnapshotChild.url = new Array<UrlSegment>(urlSegmentSecond);
    activatedRouteSnapshot = { ...activatedRouteSnapshot,
      component: undefined,
      data: undefined,
      fragment: "",
      outlet: "",
      params: undefined,
      queryParams: undefined,
      routeConfig: undefined,
      get children(): ActivatedRouteSnapshot[] {
        return [];
      },
      get firstChild(): ActivatedRouteSnapshot | null {
        return undefined;
      },
      get paramMap(): ParamMap {
        return undefined;
      },
      get parent(): ActivatedRouteSnapshot | null {
        return undefined;
      },
      get pathFromRoot(): ActivatedRouteSnapshot[] {
        return [activatedRouteSnapshotSelf, activatedRouteSnapshotChild];
      },
      get queryParamMap(): ParamMap {
        return undefined;
      },
      get root(): ActivatedRouteSnapshot {
        return undefined;
      },
      toString(): string {
        return "";
      },
      url: [urlSegment]
    };
    // when
    const result: string = resolve.getResolvedUrl(activatedRouteSnapshot);
    // then
    expect(resolve).toBeTruthy();
    expect(result).toBe("route1/route2");
  });
});
