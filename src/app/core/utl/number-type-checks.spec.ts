import {NumberTypeChecks} from "./number-type-checks";

describe("[NumberTypeChecks] tests for different number type checks", () => {
  it("should create an instance", () => {
    expect(new NumberTypeChecks()).toBeTruthy();
  });

  it("check if input fits regex mask", () => {
    // given
    const numberTypeChecks: NumberTypeChecks = new NumberTypeChecks();
    const numberAsString: string = "1.2";

    // when
    const isNumber: boolean = numberTypeChecks.isNumber(numberAsString);

    // then
    expect(isNumber).toBe(true);
  });

  it("check if invalid input fits regex mask", () => {
    // given
    const numberTypeChecks: NumberTypeChecks = new NumberTypeChecks();
    const numberAsString: string = "1.2,2";

    // when
    const isNumber: boolean = numberTypeChecks.isNumber(numberAsString);

    // then
    expect(isNumber).toBe(false);
  });

  it("check if null input fits regex mask", () => {
    // given
    const numberTypeChecks: NumberTypeChecks = new NumberTypeChecks();
    const numberAsString: string = null;

    // when
    const isNumber: boolean = numberTypeChecks.isNumber(numberAsString);

    // then
    expect(isNumber).toBe(false);
  });
});
