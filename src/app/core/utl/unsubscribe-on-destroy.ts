import {Subject} from "rxjs";
import {Directive, OnDestroy} from "@angular/core";

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class UnsubscribeOnDestroy implements OnDestroy {
  protected componentDestroyed: Subject<void>;

  protected constructor() {
    this.componentDestroyed = new Subject<void>();
  }

  ngOnDestroy(): any {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
  }
}
