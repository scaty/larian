import {AbstractControl, FormArray, FormGroup} from "@angular/forms";
import {Choice} from "../models/choice";

/**
 * String Constants class to help with unified static strings
 */
export class StringConstants {
  public static DELETE_SUCCESS: string = "Erfolgreich gelöscht!";
  public static SAVE_SUCCESS: string = "Erfolgreich gespeichert!";
  public static UPDATE_SUCCESS: string = "Erfolgreich aktualisiert!";
  public static OK: string = "OK";
}

export function shallowCopy<T>(object: T): T {
  if ( Array.isArray(object)) {
    return Object.assign([], object);
  } else {
    return Object.assign({}, object);
  }
}

// tslint:disable-next-line:typedef
export const deepCopy = <T>(target: T): T => {
  if (target === null) {
    return target;
  }
  if (target instanceof Date) {
    return new Date(target.getTime()) as any;
  }
  if (target instanceof Array) {
    const cp: any[] = [] as any[];
    (target as any[]).forEach(v => { cp.push(v); });
    return cp.map((n: any) => deepCopy<any>(n)) as any;
  }
  if (typeof target === "object" && target !== {}) {
    const cp: {} = { ...(target as { [key: string]: any }) } as { [key: string]: any };
    Object.keys(cp).forEach(k => {
      cp[k] = deepCopy<any>(cp[k]);
    });
    return cp as T;
  }
  return target;
};

export function roundToTwo(num: number): number {
  const numValue: number = +(num + "e+2");
  return +(Math.round(numValue)  + "e-2");
}
export function truncToTwo(num: number): number {
  const re: RegExp = new RegExp("(\\d+\\.\\d{" + 2 + "})(\\d)");
  const m: RegExpMatchArray = num.toString().match(re);
  return m ? parseFloat(m[1]) : num;
}

export function triggerValidation(control: AbstractControl): void {
  if (control instanceof FormGroup) {
  const group: FormGroup = control;

  // tslint:disable-next-line:forin
  for (const field in group.controls) {
    const c: any = group.controls[field];

    triggerValidation(c);
  }
  } else if (control instanceof FormArray) {
    const group: FormArray = control;

    // tslint:disable-next-line:forin
    for (const field of group.controls) {
      const c: AbstractControl = field;
      c.markAllAsTouched();
      this.triggerValidation(c);
    }
  }

  control.updateValueAndValidity({ onlySelf: false });
}
export function distinctBy<T, U extends string | number>(array: T[], mapFn: (el: T) => U): T[] {
  const uniqueKeys: Set<U> = new Set(array.map(mapFn));
  return array.filter(el => uniqueKeys.has(mapFn(el)));
}
export function studyShortTitle(choice: Array<Choice>, dgprograms: string): string {
  return choice.find(o => o.uuid === dgprograms) ? choice.find(o => o.uuid === dgprograms).choiceText : "";
}
