import {TestBed} from "@angular/core/testing";

import {AuthenticationGuard} from "./authentication-guard.service";
import {HttpClientModule} from "@angular/common/http";
import {CoreModule} from "../core.module";
import {AmStoreModule} from "../../store/am-store.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {RouterTestingModule} from "@angular/router/testing";
import {AuthenticationService} from "../../shared/services/authentication.service";
import {TransferState} from "@angular/platform-browser";
import {CookieService} from "../../shared/services/cookie.service";
import {LoaderService} from "../../shared/services/loader.service";

describe("AuthenticationGuardGuard", () => {
  let guard: AuthenticationGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, CoreModule, AmStoreModule, BrowserAnimationsModule, RouterTestingModule.withRoutes([])],
      providers: [ AuthenticationService, { provide: "req", useValue: null }, TransferState, CookieService, LoaderService]
    });
    guard = TestBed.inject(AuthenticationGuard);
  });

  it("should be created", () => {
    expect(guard).toBeTruthy();
  });
});
