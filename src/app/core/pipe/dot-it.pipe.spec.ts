import {DotItPipe} from "./dot-it.pipe";

describe("[DotItPipe] Pipe to convert string with decimal comma into string with dot separation", () => {
  it("check if dotItPipe can be created", () => {
    const pipe: DotItPipe = new DotItPipe();
    expect(pipe).toBeTruthy();
  });
  it("string 2,3 should return 2.3", () => {
    // given
    const pipe: DotItPipe = new DotItPipe();
    const stringToDotIt: string = "2,3";

    // when
    const result: string = pipe.transform(stringToDotIt);

    // then
    expect(result).toBe("2.3");
  });

  it("string ,3 should return ,3", () => {
    // given
    const pipe: DotItPipe = new DotItPipe();
    const stringToDotIt: string = ",3";

    // when
    const result: string = pipe.transform(stringToDotIt);

    // then
    expect(result).toBe(".3");
  });

  it("string , should return ,", () => {
    // given
    const pipe: DotItPipe = new DotItPipe();
    const stringToDotIt: string = ",";

    // when
    const result: string = pipe.transform(stringToDotIt);

    // then
    expect(result).toBe(".");
  });
});
