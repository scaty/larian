export class HelpPage {
  public uuid: string | undefined;
  public identifier: string | undefined;
  public helpTitle: string | undefined;
  public helpText: string | undefined;
}
