export class Choice {
  public uuid: string | undefined;
  public identifier: string | undefined;
  public title: string | undefined;
  public titleEn: string | undefined;
}
