export class PrintNode {
  id: string;
  uuid: string;
  title: string;
  titleEn: string;
  term: number;
  locationInCurriculum: Array<number>;
  color: string;
  type: string;
  position: number;
  children: Array<PrintNode>;
}
