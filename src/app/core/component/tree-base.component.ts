import {UnsubscribeOnDestroy} from "../core/utl/unsubscribe-on-destroy";
import {PrintNode} from "../core/models/print-node";
import {Observable, of} from "rxjs";
import {FlatTreeControl} from "@angular/cdk/tree";
import {MatTreeFlatDataSource, MatTreeFlattener} from "@angular/material/tree";
import {EntityCollectionService} from "@ngrx/data/src/entity-services/entity-collection-service";
import {AmEntityServices} from "./services/entity-services/am-entity.services";
import {CdkDragDrop} from "@angular/cdk/drag-drop";

// @ts-ignore
export class PrintNodeFlat {
  constructor(public expandable: boolean, public title: string, public titleEn: string, public level: number, public type: any,
              public id: string, public uuid: string, public color: string, public position: number) {}
}
export class TreeBaseComponent extends UnsubscribeOnDestroy {
  protected readonly printStudyService: EntityCollectionService<PrintNode>;

  treeControl: FlatTreeControl<PrintNodeFlat>;
  treeFlattener: MatTreeFlattener<PrintNode, PrintNodeFlat>;
  dataSource: MatTreeFlatDataSource<PrintNode, PrintNodeFlat>;
  constructor(private readonly amEntityServicesParent: AmEntityServices) {
    super();
    this.printStudyService = this.amEntityServicesParent.printStudyService;
    this.treeFlattener = new MatTreeFlattener(
      this.transformer, this.getLevel, this.isExpandable, this.getChildren);
    this.treeControl = new FlatTreeControl<PrintNodeFlat>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  }

  expandedNodeSet: Set<string> = new Set<string>();

  transformer = (node: PrintNode, level: number): PrintNodeFlat => {
    return new PrintNodeFlat(!!node.children, node.title, node.titleEn, level, node.type, node.id, node.uuid, node.color, node.position);
  };
  protected readonly getLevel: (node: PrintNodeFlat) => number = (node: PrintNodeFlat) => node.level;
  protected readonly isExpandable: (node: PrintNodeFlat) => boolean = (node: PrintNodeFlat) => node.expandable;
  protected readonly getChildren: (node: PrintNode) => Observable<PrintNode[]> =
    (node: PrintNode): Observable<PrintNode[]> => of(node.children);
  hasChild = (a: number, nodeData: PrintNodeFlat): boolean => nodeData.expandable;

  visibleNodes(): PrintNode[] {
    const result: any[] = [];

    function addExpandedChildren(node: PrintNode, expanded: Set<string>): void {
      result.push(node);
      if (expanded.has(node.id)) {
        node.children.forEach(child => addExpandedChildren(child, expanded));
      }
    }

    this.dataSource.data.forEach(node => {
      addExpandedChildren(node, this.expandedNodeSet);
    });
    return result;
  }

  protected findNodeSiblings(arr: Array<any>, id: string): Array<any> {
    let result: any;
    let subResult: any;
    arr.forEach(item => {
      if (item.id === id) {
        result = arr;
      } else if (item.children) {
        subResult = this.findNodeSiblings(item.children, id);
        if (subResult) {
          result = subResult;
        }
      }
    });
    return result;
  }

  protected findNode(arr: Array<any>, ids: Array<string>): Array<PrintNode> {
    const result: Array<PrintNode> = new Array<PrintNode>();
    arr.forEach(item => {
      if (ids.some(o => o === item.id)) {
        result.push(item);
      }
      if (item.children) {
        const subResult: Array<PrintNode> = this.findNode(item.children, ids);
        if (subResult.length > 0) {
          for (const x of subResult) {
            result.push(x);
          }
        }
      }
    });
    return result;
  }

  rebuildTreeForData(data: Array<PrintNode>, level: number = 999): void {
    this.rememberExpandedTreeNodes(this.treeControl, this.expandedNodeSet, level);
    this.dataSource.data = data;
    this.forgetMissingExpandedNodes(this.treeControl, this.expandedNodeSet);
    this.expandNodesById(this.treeControl.dataNodes, Array.from(this.expandedNodeSet));
  }

  protected rememberExpandedTreeNodes(treeControl: FlatTreeControl<PrintNodeFlat>, expandedNodeSet: Set<string>,
                                      level: number = 999): void {
    if (treeControl.dataNodes) {
      expandedNodeSet.clear();
      treeControl.dataNodes.forEach(node => {
        if (treeControl.isExpandable(node) && treeControl.isExpanded(node) && node.level < level) {
          expandedNodeSet.add(node.id);
        }
      });
    }
  }

  private forgetMissingExpandedNodes(treeControl: FlatTreeControl<PrintNodeFlat>, expandedNodeSet: Set<string>): void {
    if (treeControl.dataNodes) {
      expandedNodeSet.forEach(nodeId => {
        if (!treeControl.dataNodes.find(n => n.id === nodeId)) {
          expandedNodeSet.delete(nodeId);
        }
      });
    }
  }

  private expandNodesById(flatNodes: PrintNodeFlat[], ids: string[]): void {
    if (!flatNodes || flatNodes.length === 0) {
      return;
    }
    const idSet: Set<string> = new Set(ids);
    return flatNodes.forEach(node => {
      if (idSet.has(node.id)) {
        this.treeControl.expand(node);
        let parent: PrintNodeFlat = this.getParentNode(node);
        while (parent) {
          this.treeControl.expand(parent);
          parent = this.getParentNode(parent);
        }
      }
    });
  }

  protected getParentNode(node: PrintNodeFlat): PrintNodeFlat | null {
    const currentLevel: number = node.level;
    if (currentLevel < 1) {
      return null;
    }
    const startIndex: number = this.treeControl.dataNodes.indexOf(node) - 1;
    for (let i: number = startIndex; i >= 0; i--) {
      const currentNode: PrintNodeFlat = this.treeControl.dataNodes[i];
      if (currentNode.level < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }

  toggleFbr($event: boolean): void {
    for (const element of this.treeControl.dataNodes) {
      if (element.type === "study") {
        $event ? this.treeControl.expand(element) : this.treeControl.collapse(element);
      }
    }
  }

  toggleModule($event: boolean): void {
    for (const element of this.treeControl.dataNodes) {
      if (element.type === "study" || element.type === "fbr") {
        if ($event) {
          this.treeControl.expand(element);
        } else if (element.type === "fbr") {
          this.treeControl.collapse(element);
        }
      }
    }
  }

  toggleCourse($event: boolean): void {
    for (const element of this.treeControl.dataNodes) {
      if ($event) {
        this.treeControl.expand(element);
      } else if (element.type === "module") {
        this.treeControl.collapse(element);
      }
    }
  }
  showCourse($event: boolean): void {
    for (const element of this.treeControl.dataNodes) {
      if ($event) {
        element.expandable = !!this.findNode(this.dataSource.data, [element.id]).find(() => true)?.children;
      } else if (element.type === "module") {
        element.expandable = false;
      }
    }
    this.closeTree();
  }
  showModule($event: boolean): void {
    for (const element of this.treeControl.dataNodes) {
      if (element.type === "study" || element.type === "fbr") {
        if ($event) {
          element.expandable = !!this.findNode(this.dataSource.data, [element.id]).find(() => true)?.children;
        } else if (element.type === "fbr") {
          element.expandable = false;
        }
      }
    }
    this.closeTree();
  }
  showFbr($event: boolean): void {
    for (const element of this.treeControl.dataNodes) {
      if ($event) {
        element.expandable = !!this.findNode(this.dataSource.data, [element.id]).find(() => true)?.children;
      } else if (element.type === "study") {
        element.expandable = false;
      }
    }
    this.closeTree();
  }
  closeTree(): void {
    this.toggleModule(false);
    this.toggleFbr(false);
    this.toggleFbr(true);
    this.forgetMissingExpandedNodes(this.treeControl, this.expandedNodeSet);
    this.expandNodesById(this.treeControl.dataNodes, Array.from(this.expandedNodeSet));
  }
  drop(event: CdkDragDrop<string[]>, changedData: any): void {

    if (!event.isPointerOverContainer) { return; }

    const node: any = event.item.data;
    this.rememberExpandedTreeNodes(this.treeControl, this.expandedNodeSet, node.level);
    const visibleNodes: PrintNode[] = this.visibleNodes();

    const siblings: Array<any> = this.findNodeSiblings(changedData, node.id);
    const siblingIndex: number = siblings.findIndex(n => n.id === node.id);
    const nodeToInsert: PrintNode = siblings.splice(siblingIndex, 1)[0];

    const nodeAtDest: PrintNode = visibleNodes[event.currentIndex];
    if (nodeAtDest === null || nodeToInsert === null ||
      nodeAtDest === undefined || nodeToInsert === undefined) { return; }
    if (nodeAtDest.id === nodeToInsert.id) { return; }

    let relativeIndex: number = event.currentIndex; // default if no parent
    const nodeAtDestFlatNode: PrintNodeFlat = this.treeControl.dataNodes.find(n => nodeAtDest.id === n.id);
    const nodeAtSourceFlatNode: PrintNodeFlat = this.treeControl.dataNodes.find(n => nodeToInsert.id === n.id);
    const parent: PrintNodeFlat = this.getParentNode(nodeAtDestFlatNode);
    const parentSource: PrintNodeFlat = this.getParentNode(nodeAtSourceFlatNode);
    if (parent !== parentSource) { return; }
    if (parent) {
      const parentIndex: number = visibleNodes.findIndex(n => n.id === parent.id) + 1;
      relativeIndex = event.currentIndex - parentIndex;
    }
    // insert node
    const newSiblings: Array<any> = this.findNodeSiblings(changedData, nodeAtDest.id);
    if (!newSiblings) { return; }
    newSiblings.splice(relativeIndex, 0, nodeToInsert);
    this.rebuildTreeForData(changedData, node.level);
  }
  populatePosition(arr: Array<any>): void {
    const excludePositionNumbers: Array<number> = arr
      .filter(o => o.position !== null).map(o => o.position);
    arr.forEach((item, index) =>
      item.position = !excludePositionNumbers.includes(index) ? index : item.position);
    arr.sort((a, b) => a.position - b.position);
    arr.forEach(item => {
      if (item.children) {
        this.populatePosition(item.children);
      }
    });
  }
}
