import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";

import {LoginComponent} from "./login.component";
import {RouterTestingModule} from "@angular/router/testing";
import {AuthenticationService} from "../../../shared/services/authentication.service";
import {HttpClientModule} from "@angular/common/http";
import {TransferState} from "@angular/platform-browser";
import {CookieService} from "../../../shared/services/cookie.service";
import {LoaderService} from "../../../shared/services/loader.service";
import {AmStoreModule} from "../../../store/am-store.module";
import {CoreModule} from "../../core.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

describe("LoginComponent", () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [HttpClientModule, CoreModule, AmStoreModule, BrowserAnimationsModule, RouterTestingModule.withRoutes([])],
      providers: [ AuthenticationService, { provide: "req", useValue: null }, TransferState, CookieService, LoaderService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
