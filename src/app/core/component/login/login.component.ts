import {ChangeDetectorRef, Component} from "@angular/core";
import {FormControl, FormGroup} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {catchError, takeUntil} from "rxjs/operators";
import {UnsubscribeOnDestroy} from "../../utl/unsubscribe-on-destroy";
import {Observable, of} from "rxjs";

class AuthenticationService {
  obtainAccessToken(username: string, password: string): Observable<boolean> {
    return of(true);
  }
}

class LoaderService {
  setSpinning(b: boolean): void {
  }
}

@Component({
  selector: "am-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent extends UnsubscribeOnDestroy {
  form: FormGroup = new FormGroup({
    username: new FormControl(),
    password: new FormControl()
  });
  loginInvalid: boolean = false;
  loading: boolean = false;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly cd: ChangeDetectorRef,
    private readonly authService: AuthenticationService,
    private readonly loaderService: LoaderService
  ) { super(); }

  onSubmit(): void {
    this.loginInvalid = false;
    this.loading = true;
    if (this.form.valid) {
      this.loaderService.setSpinning(true);
      const username: string = this.form.get("username")?.value;
      const password: string = this.form.get("password")?.value;
      this.authService.obtainAccessToken(username, password).pipe(
        takeUntil(this.componentDestroyed),
        catchError(() => {
          this.loginInvalid = true;
          this.loaderService.setSpinning(false);
          this.loading = false;
          this.cd.markForCheck();
          return of(false);
        })).subscribe((o: boolean) => {
          this.loginInvalid = !o;
          this.loaderService.setSpinning(false);
          this.loading = false;
          this.cd.markForCheck();
      });
    }
  }
}
