import {ChangeDetectionStrategy, Component, Input} from "@angular/core";

@Component({
  selector: "am-card-section",
  templateUrl: "./card-section.component.html",
  styleUrls: ["./card-section.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardSectionComponent {

  @Input()
  editing: boolean = false;

  @Input()
  title: string = "";
}
