import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";

import {ModalComponent} from "./modal.component";
import {HttpClientModule} from "@angular/common/http";
import {CoreModule} from "../../core/core.module";
import {AmStoreModule} from "../../store/am-store.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {RouterTestingModule} from "@angular/router/testing";
import {AuthenticationService} from "../services/authentication.service";
import {TransferState} from "@angular/platform-browser";
import {CookieService} from "../services/cookie.service";
import {LoaderService} from "../services/loader.service";
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from "@angular/material/dialog";

describe("ModalComponent", () => {
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalComponent ],
      imports: [HttpClientModule, MatDialogModule,
        CoreModule, AmStoreModule, BrowserAnimationsModule, RouterTestingModule.withRoutes([])],
      providers: [ {
        provide: MatDialogRef,
        useValue: {}
      },
      {
        provide: MAT_DIALOG_DATA,
        useValue: {}
      },
        AuthenticationService, { provide: "req", useValue: null }, TransferState, CookieService, LoaderService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
