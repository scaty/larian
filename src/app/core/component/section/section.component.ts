import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnInit,
  Output,
  QueryList
} from "@angular/core";
import {UnsubscribeOnDestroy} from "../../core/utl/unsubscribe-on-destroy";
import {LoaderService} from "../services/loader.service";
import {debounceTime, filter, switchMap, takeUntil} from "rxjs/operators";
import {DialogConfigBuilder} from "../modal/dialog-config-builder";
import {MatDialog} from "@angular/material/dialog";
import {AmEntityServices} from "../services/entity-services/am-entity.services";
import {EntityCache, EntityCollectionService} from "@ngrx/data";
import {HelpPage} from "../../core/models/helppage";
import {ActivatedRoute, Router} from "@angular/router";
import {Iform} from "../../core/utl/Iform";
import {NgForm} from "@angular/forms";
import {Store} from "@ngrx/store";
import {selectHasEditPermissionsStudyByUuid} from "../../store/selectors/study.selectors";

@Component({
  selector: "am-body-section",
  templateUrl: "./section.component.html",
  styleUrls: ["./section.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SectionComponent extends UnsubscribeOnDestroy implements OnInit, OnChanges {
  private  readonly helpPageService: EntityCollectionService<HelpPage>;

  @Input()
  editing: boolean = false;

  @Input()
  exchange: boolean = false;

  @Input()
  showButtons: boolean = true;

  @Input()
  showOnlyAdd: boolean = false;

  @Input()
  showOnlySave: boolean = false;

  @Input()
  showOnlyEdit: boolean = false;

  @Input()
  headLineText: string = "No headline text was given";

  @Input()
  showPrint: boolean = false;

  @Input()
  helpIdentifier: string = "";

  @Input()
  showExchange: boolean = false;

  @Input()
  enablePrintButton: boolean = true;

  @Output()
  submitEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  editingEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  printEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  addEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  exchangeEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private readonly eRef: ElementRef,
              private readonly loaderService: LoaderService,
              private readonly matDialog: MatDialog,
              private readonly cd: ChangeDetectorRef,
              private readonly router: Router,
              private readonly route: ActivatedRoute,
              private readonly store: Store<EntityCache>,
              private readonly amEntityServices: AmEntityServices
  ) { super();  this.helpPageService = amEntityServices.helpPageService; }

  localEditing: boolean = false;
  localExchange: boolean = false;
  localHelpButton: boolean = false;

  private iWasClicked: boolean = false;
  private iRequested: boolean = false;

  @ContentChildren(Iform, {descendants: true}) iForms: QueryList<Iform>;
  @ContentChildren(NgForm, {descendants: true}) ngForms: QueryList<NgForm>;

  @HostListener("dblclick")
  clickInside(): void {
    if (!this.localEditing && this.showButtons) {
      this.toggleEdit();
    }
  }
  ngOnChanges(): void {
    this.localEditing = this.editing;
    this.localExchange = this.exchange;
    if (!this.localEditing) {
      this.iWasClicked = false;
      this.loaderService.setEditingSection(0);
    }
  }

  ngOnInit(): void {
    this.helpPageService.entities$.pipe(filter(o => !!o), takeUntil(this.componentDestroyed))
      .subscribe(o => {
        this.localHelpButton = o.some(i => i.sectionIdentifier === this.helpIdentifier);
        this.cd.markForCheck();
      });
    this.localEditing = this.editing;
    this.loaderService.getEditingSection().pipe(debounceTime(200), takeUntil(this.componentDestroyed)).subscribe(o => {
      this.checkAndShowDialog(o);
      if (this.iRequested && o === 0) {
        this.iRequested = false;
        this.iWasClicked = true;
        this.localEditing = !this.localEditing;
        this.loaderService.setEditingSection(1);
        this.editingEvent.emit(this.localEditing);
      } else if (this.iRequested && o === 3) {
        this.iRequested = false;
        this.iWasClicked = false;
        this.loaderService.setEditingSection(1);
      }
    });

    if (this.showButtons) {
      this.route.paramMap.pipe(filter( o => !!o?.get("uuid") && o?.get("uuid") !== "new"),
        switchMap(o => this.store.select(selectHasEditPermissionsStudyByUuid(o.get("uuid"), ["add_stg", "change_stg"]))),
        takeUntil(this.componentDestroyed)).subscribe(o => { this.showButtons = o; this.cd.markForCheck(); });
    }

    if (this.showPrint) {
      this.route.paramMap.pipe(filter( o => !!o?.get("uuid") && o?.get("uuid") !== "new"),
        switchMap(o => this.store.select(selectHasEditPermissionsStudyByUuid(o.get("uuid"), ["can_print_stg"]))),
        takeUntil(this.componentDestroyed)).subscribe(o => { this.showPrint = o; this.cd.markForCheck(); });
    }

  }

  private checkAndShowDialog(o: number): void {
    if (this.iWasClicked && o === 2) {
      if (this.iForms?.length > 0 && this.iForms?.find(i => i.dirty) ||
        this.ngForms?.length > 0 && this.ngForms?.find(i => i.dirty)) {
        const message: string =
          this.iForms?.find(i => i.valid && i.dirty)?.valid || this.ngForms?.find(i => i.valid && i.dirty) ?
            "Möchten Sie Änderungen unter " + this.headLineText + " speichern?" :
            "Möchten Sie Änderungen unter " + this.headLineText + " verwerfen?";

        const configBuilder: DialogConfigBuilder =
          new DialogConfigBuilder(message, this.matDialog);
        configBuilder.getDialogRef().afterClosed().pipe(takeUntil(this.componentDestroyed))
          .subscribe(res => this.changeSectionState(res) );

      } else if (this.showOnlyEdit) {
        this.editingEvent.emit(!this.editingEvent);
      } else {
        this.submitEvent.emit(true);
      }
    }
  }

  private changeSectionState(res: boolean): void {
    if (res === true && (this.iForms?.find(i => i.valid)?.valid || this.ngForms?.find(i => i.valid))) {
      this.submitEvent.emit(true);
    } else {
      this.localEditing = !res;
      this.iRequested = false;
      this.iWasClicked = true;
      this.loaderService.setEditingSection(3);
      if (res) {
        this.editingEvent.emit(this.localEditing);
      }
    }
  }

  toggleEdit(): void {
    if (!this.iWasClicked && this.loaderService.getEditingSection().value === 0) {
      this.iWasClicked = true;
      this.localEditing = !this.localEditing;
      this.loaderService.setEditingSection(1);
      this.editingEvent.emit(this.localEditing);
    } else if (!this.iWasClicked && this.loaderService.getEditingSection().value === 1) {
      this.iRequested = true;
      this.loaderService.setEditingSection(2);
    } else if (this.iWasClicked) {
      this.iWasClicked = false;
      this.iRequested = false;
      this.localEditing = !this.localEditing;
      this.loaderService.setEditingSection(0);
      this.editingEvent.emit(this.localEditing);
    }
  }

  submitForm(): void {
    this.submitEvent.emit(true);
  }

  print(): void {
    this.printEvent.emit(true);
  }

  add(): void {
    this.addEvent.emit(true);
  }

  onKeyDown($event: any): void {
    if (!this.localEditing) { return; }
    this.handleWindowsKeyEvents($event);
  }

  handleWindowsKeyEvents($event: any): void {
    if (this.showButtons) {
      const charCode: string = String.fromCharCode($event.which).toLowerCase();
      if ($event.ctrlKey && charCode === "s") {
        $event.preventDefault();
        this.submitForm();
      }
      if ($event.key === "Escape") {
        $event.preventDefault();
        this.toggleEdit();
      }
    }
  }

  toggleExchange(): void {
    this.localExchange = !this.localExchange;
    this.exchangeEvent.emit(this.localExchange);
  }
}
