import {Component, Input} from "@angular/core";

@Component({
  selector: "am-hexagon",
  templateUrl: "./hexagon.component.html",
  styleUrls: ["./hexagon.component.scss"]
})
export class HexagonComponent {
  @Input()
  color: string = "";
}
