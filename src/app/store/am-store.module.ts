import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import {NgModule} from "@angular/core";
import {environment} from "../../environments/environment";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {DefaultDataServiceConfig, EntityDataModule, EntityServices} from "@ngrx/data";
import {defaultDataServiceConfig, entityConfig} from "../shared/entity-metadata";
import {AmEntityServices} from "../shared/services/entity-services/am-entity.services";

@NgModule({
  imports: [
    StoreModule.forRoot([], {
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
        strictStateSerializability: true,
        strictActionSerializability: false,
      },
    }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([]),
    EntityDataModule.forRoot(entityConfig)
  ],
  providers: [
    AmEntityServices,
    {provide: EntityServices, useExisting: AmEntityServices},
    {provide: DefaultDataServiceConfig, useValue: defaultDataServiceConfig}, ]
})
export class AmStoreModule {}
