import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError, map} from "rxjs/operators";
import {errorObject} from "rxjs/internal-compatibility";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class APIService {

  // http options used for making API calls
  private readonly httpOptions: any;

  constructor(
    private readonly http: HttpClient,
  ) {
    this.httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        // 'Authorization': 'JWT ' + this._userService.token
      })
    };
  }

  private handleError<T>(operation: string = "operation", result?: T): (error: any) => Observable<T> {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      console.log(`${operation} failed: ${error.message}`);
      return throwError(JSON.stringify(error.statusText) as unknown as T);
      // return of(result as T);
    };
  }

  get<T>(endPoint: string): Observable<T> {
    this.setOptions();

    return this.http.get<T>(endPoint, // + "&format=" + this.APIFormat,
      this.httpOptions)
      .pipe(
        catchError(this.handleError("getAPI " + endPoint, errorObject))
      );
  }

  post<T>(endPoint: string, payLoad: T, responseType: string= ""): Observable<T> {
    this.setOptions();
    if (responseType) {
      this.httpOptions.responseType = responseType;
    }

    return this.http.post<T>( endPoint,
      JSON.stringify(payLoad), this.httpOptions)
      .pipe(
        map((data: any) => data),
        catchError(this.handleError("postAPI " + endPoint, payLoad))
      );
  }

  put<T>(endPoint: string, payLoad: object, UUID: string= ""): Observable<T> {
    this.setOptions();
    // @ts-ignore
    return this.http.put<T>( endPoint + UUID,
      JSON.stringify(payLoad),
      this.httpOptions)
      .pipe(
        map((data: any) => data),
        catchError(this.handleError("putAPI" + endPoint))
      );
  }

  delete<T>(endPoint: string, UUID: string = ""): Observable<T> {
    this.setOptions();
    // @ts-ignore
    return this.http.delete<T>( endPoint + UUID, this.httpOptions)
      .pipe(
        map((data: any) => data),
        catchError(this.handleError("deleteAPI" + endPoint))
      );
  }

  private setOptions(): void {
    this.httpOptions.headers = new HttpHeaders({"Content-Type": "application/json"});
    this.httpOptions.responseType = "json";
  }
}
