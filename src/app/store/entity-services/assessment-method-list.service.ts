import {Injectable} from "@angular/core";
import {
  EntityActionFactory,
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory,
  EntityOp
} from "@ngrx/data";
import {map} from "rxjs/operators";
import {APIService} from "../api.service";
import {Observable} from "rxjs";
import {Study} from "../../core/models/study";
import {deepCopy} from "../../core/utl/helpers";

@Injectable({ providedIn: "root" })
export class AssessmentMethodListService extends EntityCollectionServiceBase<Study> {
  constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory,
              private readonly apiService: APIService,
              private readonly entityActionFactory: EntityActionFactory) {
    super("AssessmentMethodList", serviceElementsFactory);
  }
  addMany(entities: Array<Study>): Observable<Array<Study>> {
    return this.apiService.post<Array<Study>>("/api/study/", entities)
      .pipe(map(response => {
        this.addManyToCache(deepCopy(response));
        this.dispatch(this.entityActionFactory.create<Array<Study>>(
          "Study", EntityOp.SAVE_ADD_MANY_SUCCESS, response
        ));
        return (response);
      }));
  }
}
